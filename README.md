# Timeloger


***Ссылка на курсовую работу: https://docs.google.com/document/d/1Tc10JER6nPMU1bu8sHDvYujWBkhEqqMAdkENrMH6YGE/edit***

***Ссылка на презентацию:     https://docs.google.com/presentation/d/1tIe6djqN0iNY1BY2FdkiVsWOhbaFMJHxJDBAYd3PrXU/edit?usp=sharing***

***Ссылка на видео: https://drive.google.com/drive/folders/18ASbG07WLtOv1x0zLq8yjp--hTup9zZa?usp=sharing***

***Ссылка на админку: https://my-awesome-timeloger.herokuapp.com/***


Ссылка на тз: https://docs.google.com/document/d/1gaLhJnbwB1EUIzz9LljNJLZCW0OM61r5cNxS21Y7YZM/edit

Доска с задачами: https://trello.com/invite/b/g19vNqXI/e63c19064bc8b524cf867116b4479d16/project

Ссылка на карту приложения: https://miro.com/app/board/uXjVOHnEW0I=/?invite_link_id=705723855229


***Ссылка на api: https://my-awesome-timeloger.herokuapp.com/admin/docs***

___Для того, чтобы посмотреть api надо зарегистрироваться как admin. Для этого введите username ***admin***  и password ***admin***__ 

Как запустить проекты описанно в readme в репозиториях проекта.


---

Если подмодули приложений не подгрузятся то вот ссылки на бэкенд и мобилку

бэкенд:  https://gitlab.com/Vithar1/timeloger-backend

мобилка: https://gitlab.com/Vithar1/my-awesome-project

